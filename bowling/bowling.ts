class Frame {
    throws: number[] = [];
  
    throw(pins: number) {
      this.throws.push(pins);
    }
  
    score(): number {
      return this.throws.reduce((prevN, currN) => {
        return prevN + currN;
      }, 0);
    }
  
    size(): number {
      return this.throws.length;
    }
  
    isSpare(): boolean {
      return this.throws.length >= 2 && this.throws[0] + this.throws[1] === 10;
    }
  
    isStrike(): boolean {
      return this.throws[0] === 10;
    }
  }
  
  export default class Bowling {
    rolls: number[] = [];
    frames: Frame[] = [];
    constructor(rolls: number[]) {
      for (let i = 0; i < rolls.length; i++) {
        this.roll(rolls[i]);
      }
      for (let i = 0; i < 10; i++) {
        this.frames.push(new Frame());
      }
    }
  
    roll(pins: number) {
      if (pins < 0 || pins > 10) {
        throw new Error("Pins must have a value from 0 to 10");
      }
      this.rolls.push(pins);
    }
  
    getPin(index: number): number {
      if (index < 0 || index >= this.rolls.length) {
        throw new Error("Score cannot be taken until the end of the game");
      }
      return this.rolls[index];
    }
  
    score(): number {
      let cursor: number = 0;
      let totalFrameSize: number = 0;
      for (let i = 0; i < 10; i++) {
        let frame = this.frames[i];
        frame.throw(this.getPin(cursor));
        cursor++;
        if (frame.isStrike()) {
          if(i === 9) {
            frame.throw(this.getPin(cursor));
            if(frame.score() === 20) {
              frame.throw(this.getPin(cursor + 1));
            } else {
              frame.throw(this.getPin(cursor + 1));
              if(frame.score() > 20) {
                throw new Error("Pin count exceeds pins on the lane");
              }
            }
          } else {
            frame.throw(this.getPin(cursor));
            frame.throw(this.getPin(cursor + 1));
          }
        } else {
          frame.throw(this.getPin(cursor));
          if (frame.score() > 10 && !(totalFrameSize === this.rolls.length && frame.isSpare())) {
            throw new Error("Pin count exceeds pins on the lane");
          }
          cursor++;
          if (frame.isSpare()) {
            frame.throw(this.getPin(cursor));
          }
        }
        totalFrameSize += frame.size();
      }
      if (this.rolls.length > totalFrameSize) {
        throw new Error("Should not be able to roll after game is over");
      }
  
      return this.frames.reduce((prevVal: number, frame: Frame) => {
        return prevVal + frame.score();
      }, 0);
    }
  }