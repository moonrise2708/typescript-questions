export class WordProblem {
    question: string;
    constructor(question: string) {
        this.question = question;
        
    }
    getOps(source: string): string[] {
        if (!source) {
            return [];
        }
        let result = [];
        let i = 0;
        while (i < source.length) {
            if (source.substring(i, i + 4) === "plus") {
                result.push("+");
                i += 1;
            } else if (source.substring(i, i + 5) === "minus") {
                result.push("-");
                i += 1;
            } else if (source.substring(i, i + 10) === "divided by") {
                result.push("/");
                i += 1;
            } else if (source.substring(i, i + 13) === "multiplied by") {
                result.push("*");
                i += 1;
            } else if (source.substring(i, i + 13) === "raised to the") {
                result.push("pow");
                i += 1;
            } else {
                i++;
            }
        }
        return result;
    }

    getNumbers(source: string): number[] {
        const reg = /-?\d+/g
        if (!source) {
            return [];
        }
        let result = [];
        let match;
        while ((match = reg.exec(source)) != null) {
            result.push(parseInt(match[0]))
        }
        return result;
    }

    calculator(val1: number, val2: number, op: string): number {
        switch (op) {
            case '+':
                return val1 + val2
            case '-':
                return val1 - val2
            case '*':
                return val1 * val2
            case '/':
                if(val2 !== 0) {
                    return val1 / val2
                } else {
                    return 0
                }
            case 'pow':
                return Math.pow(val1, val2)
            default:
                return 0
        }
    }

    answer() {
        if (!this.question.match(/plus|minus|multiplied by|divided by|raised to the/)) {
            throw new ArgumentError();
        }
        let numbers: number[] = this.getNumbers(this.question);
        let ops: string[] = this.getOps(this.question);
        let result: number = 0
        if(numbers.length > 0) {
            result = numbers[0]
            let i = 0
            if(ops.length > 0) {
                while(i < ops.length) {
                    result = this.calculator(result, numbers[i + 1], ops[i])
                    i++
                }
            }
        } 
        return result
    }
}
export class ArgumentError {
    
}