class CollatzConjecture {
    static step(n: number): number {
        if (n % 2 == 0) return n / 2;
        return 3 * n + 1;
    }

    static steps(n: number): number {
        if(n <= 0) {
            throw new Error('Only positive numbers are allowed')
        }
        var totalStep = 0;
        while (n != 1) {
            n = this.step(n);
            totalStep++;
        }
        return totalStep;
    }
}
export default CollatzConjecture
